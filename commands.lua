--[[

  Player comands

--]]

-------------------------------------------------------------------------------

function commandPlayerDelete(aSplit, aPlayer)
-- Delete player from private access
-- /private pl del PlayerName PrivateName

  local func_name = 'commandPlayerDelete()';

  local worldName = aPlayer:GetWorld():GetName();
  local playerName = aPlayer:GetName();
  local areaName = '';
  local removedPlayerName = '';
  local areaID = 0;


  ----------------------
  -- Messages section --
  ----------------------
  local msgNotAreaOwner = "You are not area owner. Sorry";
  local msgNotAllArguments = "Set all arguments (PlayerName PrivateName)";
  local msgNoAreaFound = "No area found for player";
  local msgPlayerRemovedOk = '';


  -- Check area name argument
  if #aSplit < 5 then
    aPlayer:SendMessageInfo(msgNotAllArguments);
    return true;
  end

  -- Get removed player name
  removedPlayerName = aSplit[4];

  msgPlayerRemovedOk = 'Player '.. removedPlayerName ..' successfully removed from area '.. areaName;

  -- Get name of private area
  areaName = aSplit[5];

  -- Check is it player owner of area
  if not IsItAreaOwner(worldName, areaName, playerName) then
    aPlayer:SendMessageFailure(msgNotAreaOwner);
    return true;
  end

  -- Get area ID
  areaID = getPrivateIDByName(worldName, areaName, playerName);

  -- Check is it area found
  if areaID < 1 then
    aPlayer:SendMessageInfo(msgNoAreaFound);
    return true;
  end

  ---------------------------
  -- Begin player deletion --
  ---------------------------
  local sql = [[
    DELETE FROM players
    WHERE login = :login AND
      area_id = :areaID
    ;
  ]];

  local stmt = private_db:prepare(sql);

  -- Check is it prepered?
  if not stmt then
    console_log(func_name .."-> stmt is nil", 2);
    Player:SendMessageInfo("Some error. Say to admin");
    return true;
  end

  -- Bind values to SQL
  stmt:bind_names({
    login = string.lower(removedPlayerName),
    areaID = areaID
  });

  -- Execute SQL
  stmt:step();

  -- Clean statement
  stmt:finalize();

  aPlayer:SendMessageSuccess(msgPlayerRemovedOk);

  -- In any case allright
  return true;
end

-------------------------------------------------------------------------------

function commandPlayerAdd(aSplit, aPlayer)
-- /private pl add PlayerName PrivateName [PermissionLevel]
-- Do not used 1 - owner
-- 2 - full access
-- 3 - use only

  local func_name = 'commandPlayerAdd()';

  local PermissionLevel = 2;  -- Default full access

  -- Setup messages
  local msgErrorLevel = "Permission level must be a number 2 or 3. Use default level ".. PermissionLevel;
  local msgAreaPermissionNotSet = "Can not setup permission";
  local msgNotAreaOwner = "You are not area owner. Sorry";

  -- Check argument count
  if #aSplit < 5 then
    aPlayer:SendMessageInfo("Set all arguments (PlayerName PrivateName [PermissionLevel])");
    return true;
  end

  -- Check is it player owner who wont change permissions
  -- IsItAreaOwner(aWorldName, aAreaName, aPlayerName)
  -- if not IsItAreaOwner(aPlayer:GetName()) then

  -- console_log(getPrivateIDByName(aPlayer:GetWorld():GetName(), aSplit[5], aPlayer:GetName()));

  -- if getPrivateIDByName(aPlayer:GetWorld():GetName(), aSplit[5], aPlayer:GetName()) < 1 then
  if not IsItAreaOwner(aPlayer:GetWorld():GetName(), aSplit[5], aPlayer:GetName()) then
    aPlayer:SendMessageFailure(msgNotAreaOwner);
    return true;
  end

  -- Setup message
  local msgPrivateNotFound = "Private area not found";

  local dstPlayerName = aSplit[4];

  -- If permission level argument setiing up - use it
  if #aSplit == 6 then

    -- Check type of argument data
    local tmpAccLevel = tonumber(aSplit[6]);

    if tmpAccLevel ~= 0 then

      -- Check access levels
      if tmpAccLevel == 2 or tmpAccLevel == 3 then
        PermissionLevel = tmpAccLevel;
      else
        aPlayer:SendMessageInfo(msgErrorLevel);
      end -- if
    else
      aPlayer:SendMessageInfo(msgErrorLevel);
    end -- if
  end -- if

  -- Setup message
  local msgPlayerGrandedOk = "Player ".. aSplit[4] .." was granded to area ".. aSplit[5] .." with permission level ".. PermissionLevel;

  -- Get private ID by name and owner
  local privateID = getPrivateIDByName(aPlayer:GetWorld():GetName(), aSplit[5], aPlayer:GetName());

  -- Check is it founded
  if privateID < 1 then
    aPlayer:SendMessageInfo(msgPrivateNotFound);
    return true;
  end

  -- setup permission
  if not areaSetPermission(privateID, dstPlayerName, PermissionLevel) then
    aPlayer:SendMessageInfo(msgAreaPermissionNotSet);
    return true;
  end

  aPlayer:SendMessageSuccess(msgPlayerGrandedOk);

  return true;
end

-------------------------------------------------------------------------------

function getPrivateIDByName(aWorldName, aPrivateName, aPlayerName)
-- Return private ID by private name and player owner name

  local func_name = 'getPrivateIDByName()';

  local sql = [[
    SELECT area.id
    FROM area, players
    WHERE area.world = :WorldName AND
          area.area_name = :PrivateName AND
          players.login = :PlayerName AND
          players.area_id = area.id AND
          players.permission_id = 1
    ;
  ]];

  -- Prepare statement
  local stmt = private_db:prepare(sql);

  -- Check is it prepered?
  if not stmt then
    console_log(func_name .."-> stmt is nil");
    return 0;
  end

  -- Bind values to SQL
  stmt:bind_names({
    WorldName = string.lower(aWorldName),
    PrivateName = string.lower(aPrivateName),
    PlayerName = string.lower(aPlayerName)
  });

  local count = 0;  -- Counter
  local areaID = 0; -- Area ID default (not founded)

  -- Get founded records
  for id1 in stmt:urows() do
    count = count + 1;
    areaID = id1;
  end

  stmt:finalize();

  -- Check multply records
  if count > 1 then
    console_log(func_name .." -> multiply areas code founded. Use last. Total count = ".. count);
  end

  return areaID;
end

-------------------------------------------------------------------------------

function commandOwnerChange(aSplit, Player)
-- Change private owner
-- /private owner <PrivateName> <PlayerName>

  local func_name = 'commandOwnerChange()';

  local newPlayerUUID = '';

  local newPlayer = nil;

  -- Check aPlayer object exists
  if not Player then
    console_log(func_name .."-> Player is nil", 2);
    return true;
  end

  -- Check params count
  if #aSplit < 4 then
    Player:SendMessageFailure("Set PrivateName and PlayerName");
    return true;
  end

  -- Check new player exists
  if not Player:GetWorld():DoWithPlayer(aSplit[4],
        function(aNewPlayer)
          newPlayerUUID = aNewPlayer:GetUUID();

          newPlayer = aNewPlayer;

          return true;
        end
      )
    then

    Player:SendMessageFailure("Player ".. aSplit[4] .." not fount.");
    return true;
  end

  --[[
  Player:GetWorld():ForEachPlayer(
    function (aNewPlayer)
      if (aNewPlayer:GetName() == aSplit[4]) then
       newPlayerUUID = aNewPlayer:GetUUID();
      end
    end
  );

  --]]

  -- Check DB connection exists
  if not private_db then
    console_log(func_name .."-> private_db is nil", 2);
    Player:SendMessageInfo("Some error. Say to admin");
    return true;
  end

  local sql = [[
    SELECT COUNT(*)
    FROM area
    WHERE uuid = :uuid AND
          area_name = :areaName
    ;
  ]];

  -- Check private name
  local stmt = private_db:prepare(sql);

  -- Check is it prepered?
  if not stmt then
    console_log(func_name .."-> stmt is nil", 2);
    Player:SendMessageInfo("Some error. Say to admin");
    return true;
  end

  -- Bind values to SQL
  stmt:bind_names({
    uuid = Player:GetUUID(),
    areaName = string.lower(aSplit[3])
  });

  local count = 0;

  for status1 in stmt:urows() do
    count = status1
  end

  stmt:finalize();

  -- Check is it find area
  if count < 1 then
    Player:SendMessageFailure("Can not find area ".. aSplit[3]);
    return true;
  elseif count > 1 then
    Player:SendMessageFailure("Find multiply areas.");
    return true;
  end

  --
  -- Area found and ready to change owner
  --

  sql = [[
    UPDATE area
    SET uuid = :newUuid
    WHERE uuid = :uuid AND
          area_name = :areaName
    ;
  ]];

  stmt = private_db:prepare(sql);

  -- Check is it prepered?
  if not stmt then
    console_log(func_name .."-> stmt is nil into UPDATE", 2);
    Player:SendMessageInfo("Some error. Say to admin");
    return true;
  end

  -- Bind values to SQL
  stmt:bind_names({
    uuid = Player:GetUUID(),
    areaName = string.lower(aSplit[3]),
    newUuid = newPlayer:GetUUID()
  });

  stmt:step();

  --[[
  -- Che is it all right?
  if stmt ~= sqlite3.DONE and stmt ~= sqlite3.ROW then
    console_log(func_name .."-> stmt ~= sqlite3.DONE and stmt ~= sqlite3.ROW. stmt = ".. tostring(stmt), 2);
    Player:SendMessageInfo("Some error. Say to admin");
    return true;
  end

  --]]

  stmt:finalize();

  Player:SendMessageSuccess("Owner for area ".. aSplit[3] .." changed to player ".. newPlayer:GetName());
  newPlayer:SendMessageSuccess("You get new private area ".. aSplit[3] .." from player ".. Player:GetName());

  return true;
end

-------------------------------------------------------------------------------

function CommandMark(Split, Player)
  -- Begin mark square.

  if not Player then
    return false
  end

  local ret = nil

  --
  -- Does we have the record (player run command some time)
  --
  if is_mark_exists(Player) then
    Player:SendMessageInfo("Выделение территории уже активировано")
    return true
  end

  -- All right - create record in temp DB
  -- g_tmp_db:exec()
  local stmt = g_tmp_db:prepare("INSERT INTO data(world, uuid, mark) VALUES(?, ?, ?);")

  if not stmt then
    Player:SendMessage("Some error. Can\'t mark area. Talk admin")
    console_log("Can\'t prepare SQL statement to create marked record")
    return true
  end

  --
  -- Bind values for SQL statements
  --
  if stmt:bind(1, string.lower(Player:GetWorld():GetName())) ~= sqlite3.OK then
    Player:SendMessage("Some error. Can\'t mark area. Talk admin")
    console_log("Can\'t bind value #1 for SQL statement to create marked record")
    return true
  end

  if stmt:bind(2, Player:GetUUID()) ~= sqlite3.OK then
    Player:SendMessage("Some error. Can\'t mark area. Talk admin")
    console_log("Can\'t bind value #2 for SQL statement to create marked record")
    return true
  end

  if stmt:bind(3, 1) ~= sqlite3.OK then
    Player:SendMessage("Some error. Can\'t mark area. Talk admin")
    console_log("Can\'t bind value #3 for SQL statement to create marked record")
    return true
  end

  --
  -- Try to execute SQL statement
  --

  ret = stmt:step()

  if ret ~= sqlite3.DONE then
    Player:SendMessage("Some error. Can\'t mark area. Talk admin")
    console_log("Can\'t execute stmt:step() for SQL statement to create marked record. Error code: " .. ret)
    return true
  end

  if stmt:finalize() ~= sqlite3.OK then  -- Finish him!
    Player:SendMessage("Some error. Can\'t mark area. Talk admin")
    console_log("Can\'t stmt:finalize() for SQL statement to create marked record")
    return true
  end

  -- Talk with player
  Player:SendMessageSuccess("Выделение привата активировано. Используйте левую кнопку мышки для указания первой точки и правую кнопку мышки - для указания второй точки области привата. После окончания выделения области - выполните команду /private save или /wand save.")

  return true
end

-------------------------------------------------------------------------------

function CommandList(Split, Player)
-- List player's areas.

  -- Check Player object not emty
  if not Player then
    console_log("AreaList empty Player object")
    player_message_error(Player)
    return true
  end

  local playerName = Player:GetName();

  -- Get areas names
  sql = [=[
    SELECT id, area_name, area_size
    FROM area
    WHERE world = :world AND
          uuid = :uuid
    ;
  ]=]

  local stmt = private_db:prepare(sql)

  if not stmt then
    console_log("Error. AreaList() -> private_db:prepare(".. sql ..")")
    return true
  end

  -- Bind variables
  local ret = stmt:bind_names({
     world = string.lower(Player:GetWorld():GetName()),
     uuid = Player:GetUUID()
  })

  local area_name = nil
  local area_size = nil

  -- Table for save list
  local areasList = {};

  -- Counter
  local i = 2;

  -- Presetup table header
  areasList[1] = "\nID | Name | Size";

  -- Create players list
  for id, area_name, area_size in stmt:urows() do
    -- Player:SendMessageInfo(area_name .."  |  ".. area_size)
    areasList[i] = id .. " | ".. area_name .." | ".. area_size;

    i = i + 1;
  end

  -- Clean statement
  if stmt:finalize() ~= sqlite3.OK then  -- Finish him!
    console_log("Error. AreaList() -> stmt:finalize()")
    return true
  end


  ----------------------------------------------
  -- Create list from new tables of privates ---
  ----------------------------------------------

  local sql = [[
    SELECT area.id, area.area_name, area.area_size
    FROM area, players
    WHERE players.login = :login AND
          area.id = players.area_id AND
          area.world = :world AND
          permission_id = 1
    ;
  ]];

  -- Prepare statement
  local stmt = private_db:prepare(sql);

  if not stmt then
    console_log("Error. AreaList() -> private_db:prepare(".. sql ..")")
    return true
  end

  -- console_log(playerName);

  -- Bind values
  local ret = stmt:bind_names({
    world = string.lower(Player:GetWorld():GetName()),
    login = string.lower(playerName)
  })

  -- Execute statemen and get data
  for id, area_name, area_size in stmt:urows() do
    -- Player:SendMessageInfo(area_name .."  |  ".. area_size)
    areasList[i] = id .. " | ".. area_name .." | ".. area_size;

    i = i + 1;
  end

  -- Clean statement
  if stmt:finalize() ~= sqlite3.OK then  -- Finish him!
    console_log("Error. AreaList() -> stmt:finalize()")
    return true
  end

  -- Show table content
  if i > 2 then
    Player:SendMessageInfo(table.concat(areasList, "\n"));
  else
    Player:SendMessageInfo("No privates for you");
  end

  return true
end -- CommandList

-------------------------------------------------------------------------------

-- :%s/\s\+$//g

