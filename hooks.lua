--[[

  Hooks for private

--]]

function hooks()
-- Setup hooks

  cPluginManager.AddHook(cPluginManager.HOOK_PLAYER_LEFT_CLICK, MyOnPlayerLeftClick);
  cPluginManager.AddHook(cPluginManager.HOOK_PLAYER_RIGHT_CLICK, MyOnPlayerRightClick);
  cPluginManager.AddHook(cPluginManager.HOOK_PLAYER_RIGHT_CLICKING_ENTITY, OnPlayerRightClickingEntity);

  if pluginSettings['DemageProtect']:lower() == 'true' then
    LOG('Damage protect activated')
    cPluginManager.AddHook(cPluginManager.HOOK_TAKE_DAMAGE, OnTakeDamage);
  else
    LOG('Damage protect turned off')
  end

end
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

-- :%s/\s\+$//g
--------------------------------------------------------------------------------

