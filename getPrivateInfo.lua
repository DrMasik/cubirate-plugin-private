--
-- getPrivateInfo
--
--------------------------------------------------------------------------------

function getPrivateInfo(aWorldName, aPosition)

  local func_name = 'getPrivateInfo()';
  local privateID = nil;
  local ownerName = nil;
  local position = aPosition;
  local worldName = aWorldName:lower();

  local sql = [[
    SELECT area.id, players.login
    FROM area, players
    WHERE area.world = :worldName
          AND ((area.x1 <= :x1 AND area.z1 >= :z1) AND (area.x3 >= :x1 AND area.z3 <= :z1))
          AND players.area_id = area.id
          AND players.permission_id = 1
    ;
  ]];

  -- Prepare statement to query
  local stmt = private_db:prepare(sql);

  -- Is it prpared?
  if not stmt then
    console_log("Error. ".. func_name .." -> private_db:prepare(".. sql ..")");
    return (-1);
  end

  -- Bind variables
  local ret = stmt:bind_names({
      x1 = position.x,
      z1 = position.z,
      worldName = worldName
   });

  -- Get private ID
  for id1, playerName1 in stmt:urows() do
    privateID = id1;
    ownerName = playerName1;
  end

  -- Clean stmt var
  stmt:finalize();

  return privateID, ownerName;
end
