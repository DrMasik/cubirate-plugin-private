--
-- loadConfig()
--
-- Загружает конфигурацию плагина
--------------------------------------------------------------------------------

function loadConfig()

  local pluginDir = PLUGIN:GetLocalFolder();

  -- Check config file exists
  if cFile:IsFile(pluginDir ..'/config.ini') == false then
    -- File does not exists - create default
    LOG('Config file does not exists. Create it.');
    createIniFile(pluginDir, 'config.ini');
  end

  LOG('Load plugin settings');
  if loadPluginSettings() ~= true then
    return false
  end

  return true
end

