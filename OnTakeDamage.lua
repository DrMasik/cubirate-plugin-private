--
-- OnTakeDamage()
--
--------------------------------------------------------------------------------

function OnTakeDamage(aReceiver, aTDI)

  local func_name = 'OnTakeDamage()';

  -- Is it player?
  if (aTDI.Attacker == nil or not aTDI.Attacker:IsPlayer()) then
    return false
  end

  local position = aReceiver:GetPosition();
  local worldName = aReceiver:GetWorld():GetName();
  local worldObj = cRoot:Get():GetWorld(worldName);
  local playerObj = nil;

  local privateID, privateOwnerName = getPrivateInfo(worldName, position);

  -- Is it private exists?
  if privateID == nil or privateID == 0 or privateID == false then
    return false;
  end

  -- Get player name (attaker)
  local playerUID = aTDI.Attacker:GetUniqueID();

  -- Find player
  worldObj:DoWithEntityByID(playerUID,
    function(aEntity)
      playerObj = aEntity;

      return true;
    end
  );

  -- Always ~= nil. But check
  if playerObj == nil then
    return false;
  end

  if privateOwnerName == playerObj:GetName():lower() then
    return false;
  end

  -- Disable damage
  return true;
end
--------------------------------------------------------------------------------

