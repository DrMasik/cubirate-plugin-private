-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------

function loadPluginSettings()
  local func_name = 'cSystem:loadPluginConfig()';

  local IniFile = cIniFile();

  local aDir = PLUGIN:GetLocalFolder();
  local aFileName = 'config.ini';

  if IniFile:ReadFile(aDir ..'/'.. aFileName) == false then
    return false;
  end

  pluginSettings['DemageProtect'] = IniFile:GetValue("General", "DemageProtect");

  return true;
end

--------------------------------------------------------------------------------

