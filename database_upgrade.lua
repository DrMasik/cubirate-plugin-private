--[[

CREATE TABLE area1(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      world text, uuid text,
      x1 integer, y1 integer, z1 integer,
      x2 integer, y2 integer, z2 integer,
      x3 integer, y3 integer, z3 integer,
      x4 integer, y4 integer, z4 integer,
      area_size integer, area_name text
    );

INSERT INTO area1(world, uuid, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, area_size, area_name)
SELECT world, uuid, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, area_size, area_name
FROM area;

ALTER TABLE area RENAME TO area2;
ALTER TABLE area1 RENAME TO area;

--------------------------------------
--------- Branch permittions ---------
--------------------------------------

CREATE TABLE "players"(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      area_id INTEGER,
      world TEXT,
      login TEXT,
      permission_id INTEGER
    );

CREATE INDEX IF NOT EXISTS players_world_login
ON players(world, login);

CREATE INDEX IF NOT EXISTS players_world_login_permission_id
ON players(world, login, permission_id);

CREATE TABLE "permissions"(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      description_en TEXT,
      description_ua TEXT,
      description_ru TEXT
    );

CREATE INDEX players_login_permission_id
ON players(login, permission_id);

---------------------------------------
----- Update config table
----- Add player name to table VS UUID
---------------------------------------

ALTER TABLE config
ADD COLUMN login TEXT;

CREATE INDEX IF NOT EXISTS config_login
ON config(login);

CREATE INDEX IF NOT EXISTS config_login_world
ON config(login, world);

----------------------------------------------
-- Add critical data into permissions table --
----------------------------------------------

INSERT INTO permissions(description_en) VALUES('Owner');
INSERT INTO permissions(description_en) VALUES('Full');
INSERT INTO permissions(description_en) VALUES('Use');

--]]

-- :%s/\s\+$//g

