-- =============================================================================
--
-- Written by DrMasik
--
-- The best Minecraft server
-- www.cuberite.org
-------------------------------------------------------------------------------

function createIniFile()
  local func_name = 'createIniFile()';

  local IniFile = cIniFile();

  local aDir = PLUGIN:GetLocalFolder();
  local aFileName = 'config.ini';

  IniFile:AddValue("General", "DemageProtect", 'true'); -- Защищаем от получения урона в зоне привата

  if IniFile:WriteFile(aDir ..'/'.. aFileName) == false then
    return false;
  end

  return true;
end
--------------------------------------------------------------------------------


