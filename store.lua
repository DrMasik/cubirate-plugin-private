--------------------------------------------------------------------------------

function IsItAreaOwner(aWorldName, aAreaName, aPlayerName)
-- Check is it player owner of area

  local func_name = 'IsItAreaOwner()';

  -- Get area ID
  local areaID = getPrivateIDByName(aWorldName, aAreaName, aPlayerName);

  -- Is it found?
  if areaID < 1 then
    return false;
  end

  local sql = [[
    SELECT COUNT(*)
    FROM players
    WHERE login = :login AND
          area_id = :areaID AND
          permission_id = 1
    ;
  ]];

  local stmt = private_db:prepare(sql);

  -- Check is it prepered?
  if not stmt then
    console_log(func_name .."-> stmt is nil", 2);
    Player:SendMessageInfo("Some error. Say to admin");
    return false;
  end

  -- Bind values to SQL
  stmt:bind_names({
    login = string.lower(aPlayerName),
    areaID = areaID
  });

  local count = 0;

  -- Get calculate count
  for status1 in stmt:urows() do
    count = status1
  end

  -- Clean statement
  stmt:finalize();

  -- Is it found somthing?
  if count < 1 then
    return false;
  end

  return true;
end

--------------------------------------------------------------------------------

