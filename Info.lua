-------------------------------------------------------------------------------
-- Info.lua
-- Private
-------------------------------------------------------------------------------

g_PluginInfo = {
  Name = "Private",
  Version = "2016090102",
  Date = "2016-09-01-02",
  Description = [[Private territory]],

  Commands = {
    ["/private"] = {
      HelpString = "Дейстивя с приватом",
      Permission = "private.core",

      Subcommands = {
        info = {
          Subcommands = {
            on = {
              HelpString = "Включить режим информации",
              Handler = commandGUIInfoOn,
            }, -- on

            off = {
              HelpString = "Вы�ключить режим информации",
              Handler = commandGUIInfoOff,
            }, -- off
          }, -- Subcommands
        }, -- info

        cancel = {
          HelpString = "Отменить выделение привата",
          Handler = CommandCancel,
        },

        mark = {
          HelpString = "Начать выделение привата",
          Handler = CommandMark,
          Permission = "private.mark"
        },

        list = {
          HelpString = "Показать список приватов",
          Handler = CommandList,
        },

        del = {
          HelpString = "Удалить приват по названию",
          Handler = CommandDelete,
          ParameterCombinations = {
            Params = "name",
            HelpString = "Название привата",
          },
        },

        --[[
        owner ={
          HelpString = 'Change private owner.',
          Handler = commandOwnerChange,
          ParameterCombinations = {
            Params = "PlayerName",
            HelpString = 'New owner player's name',
          },
        },
        --]]

        --[[
        merge = {
          HelpString = "Merge two private territory",
          Handler = CommandMerge,
        },
        -- ]]

        --[[
        split = {
          HelpString = "Split into two parts",
          Handler = CommandSplit,
        },
        --]]

        save = {
          HelpString = "Сохранить помеченную область как приват",
          Handler = CommandSave,
          Permission = "private.save"
        },

        pl = {
          HelpString = "Manage players <add|del>",
          Subcommands = {
            add = {
              HelpString = "Add player to private",
              Handler = commandPlayerAdd,
              ParameterCombinations = {
                {
                  Params = "PlayerName PrivateName/ID [PermLevel]. PermLevel = <2|3>",
                  HelpString = "Player name to private name access ",
                },
              }, -- ParameterCombinations
            },  -- add

            del = {
              HelpString = "Delete players from private",
              Handler = commandPlayerDelete,
              ParameterCombinations = {
                {
                  Params = "PlayerName PrivateName/ID",
                  HelpString = "Player name to delete from private access",
                },
              }, -- ParameterCombinations
            },  -- del
          },  -- Subcommands
        },  -- /private pl
      },
    },

    ["/wand"] = {
      HelpString = "Начать выделение привата",
      Handler = CommandMark,
      Permission = "private.mark",
      Subcommands = {
        cancel = {
          HelpString = "Отменить выделение привата",
          Handler = CommandCancel,
        },
        save = {
          HelpString = "Сохранить помеченную область как приват",
          Handler = CommandSave,
          Permission = "private.save"
        },
      },
    },
  },

  ------------------------------------------------------------------------------------------
  -- Console commands
  ------------------------------------------------------------------------------------------
  ConsoleCommands =
  {
    -- ["private"] ={
    --   HelpString = "Work with private plugin",
    --   Subcommands = {
    --     dp = {
    --       HelpString = "Configure damage protect setting",
    --       Handler = commandConsoleDamageProtect,
    --     },  -- dp
    --   },  -- Subcommands
    -- },  -- "private"
  },


  ------------------------------------------------------------------------------------------
  -- Permissions section
  ------------------------------------------------------------------------------------------
  Permissions =
  {
  },
}

