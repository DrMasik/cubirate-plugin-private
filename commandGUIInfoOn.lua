--
-- commandGUIInfoOn()
--
--------------------------------------------------------------------------------

gInfo = {};
--------------------------------------------------------------------------------

function commandGUIInfoOn(aSprit, aPlayer)
  local func_name = 'commandGUIInfoOn()';

  if #aSprit < 3 then
    return true;
  end

  if aPlayer == nil then
    return true;
  end

  local plUID = aPlayer:GetUniqueID();

  gInfo[plUID] = true;

  aPlayer:SendMessageSuccess(msgInfoIsOn);

  return true;
end
--------------------------------------------------------------------------------

