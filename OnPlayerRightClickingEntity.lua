--
-- OnPlayerRightClickingEntity()
--
--------------------------------------------------------------------------------

function OnPlayerRightClickingEntity(aPlayer, aEntity)
  local func_name = 'OnPlayerRightClickingEntity()';

  local worldName = aPlayer:GetWorld():GetName();
  local position = Vector3i(aPlayer:GetPosX(), aPlayer:GetPosY(), aPlayer:GetPosZ());

  local privateID, privateOwnerName = getPrivateInfo(worldName, position);

  -- Is it private exists?
  if privateID == nil or privateID == 0 or privateID == false then
    return false;
  end

  -- Is it owner of private?
  if privateOwnerName == aPlayer:GetName():lower() then
    return false;
  end

  -- Disable right click
  return true;
end

