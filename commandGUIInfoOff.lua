--
-- commandGUIInfoOff()
--
--------------------------------------------------------------------------------

function commandGUIInfoOff(aSprit, aPlayer)
  local func_name = 'commandGUIInfoOff()';

  if #aSprit < 3 then
    return true;
  end

  if aPlayer == nil then
    return true;
  end

  local plUID = aPlayer:GetUniqueID();

  gInfo[plUID] = nil;

  aPlayer:SendMessageSuccess(msgInfoIsOff);

  return true;
end
--------------------------------------------------------------------------------

