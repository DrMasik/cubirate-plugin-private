--
-- showRegionInfo()
--
--------------------------------------------------------------------------------

function showRegionInfo(aPlayerObj, aPosition)
  -- Show region information

  local func_name = 'showRegionInfo()';

  if aPlayerObj == nil or aPosition == nil then
    return false;
  end

  local playerObj = aPlayerObj;
  local position = aPosition;
  local worldName = playerObj:GetWorld():GetName():lower();

  -- Get area ID, wold name, area name
  local sql = [=[
    SELECT area.id, area.area_name, players.login
    FROM area, players
    WHERE area.world = :world AND
          ((area.x1 <= :x1 AND area.z1 >= :z1) AND (area.x3 >= :x1 AND area.z3 <= :z1))
          AND players.area_id = area.id
          AND players.permission_id = 1
    ;
  ]=];

  -- Get Owner name
  local sql1 = [[
  ]];

  -- Prepare statement to query
  local stmt = private_db:prepare(sql);

  -- Is it prpared?
  if not stmt then
    console_log("Error. ".. func_name .." -> private_db:prepare(".. sql ..")");
    return (-1);
  end

  -- Bind variables
  local ret = stmt:bind_names({
      x1 = position.x,
      z1 = position.z,
      world = worldName
   });

  -- local id = 0;  -- Private ID
  -- local idsCount = 0; -- Count of ID. Must be always 1 or 0

  -- Get private ID
  for id1, area_name1, playerOwnerName1 in stmt:urows() do
    playerObj:SendMessage(cCompositeChat()
      :AddTextPart(id1 .." ".. area_name1 .." ".. playerOwnerName1)
    );
  end

  -- console_log("Debug: ".. func_name .."-> private id = ".. tostring(id) .."; login = '".. Player:GetName() .."'; idsCount = ".. tostring(idsCount));

  -- Clean stmt var
  stmt:finalize();

  return true;
end
--------------------------------------------------------------------------------

